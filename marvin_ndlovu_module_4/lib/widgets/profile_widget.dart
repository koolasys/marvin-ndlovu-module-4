import 'package:flutter/material.dart';

class ProfileWidget extends StatelessWidget {
  const ProfileWidget({
    Key? key,
    required this.imgPath,
    required this.onClicked,
    this.isEdit = false,
  }) : super(key: key);
  final String imgPath;
  final VoidCallback onClicked;
  final bool isEdit;

  /* Building the profile imae widget */
  ///Profile photo widget
  Widget buildImage() {
    final image = NetworkImage(imgPath);

    return ClipOval(
      child: Material(
        color: Colors.transparent,
        child: Ink.image(
          image: image,
          fit: BoxFit.cover,
          width: 130,
          height: 130,
          child: InkWell(onTap: onClicked),
        ),
      ),
    );
  }

  /// Edit display picture icon
  Widget buildEditDPIcon(Color color) {
    return buildIconCircle(
      color: Colors.white70,
      all: 2,
      child: buildIconCircle(
        color: color,
        all: 8,
        child: Icon(
          isEdit ? Icons.add_a_photo : Icons.edit,
          size: 20,
        ),
      ),
    );
  }

  ///Circle around the edit DP icon
  Widget buildIconCircle(
      {required Widget child, required double all, required Color color}) {
    return ClipOval(
      child: Container(
        color: color,
        padding: EdgeInsets.all(all),
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).colorScheme.primary;

    return Center(
      child: Stack(
        children: [
          buildImage(),
          Positioned(bottom: 0, right: 4, child: buildEditDPIcon(color)),
        ],
      ),
    );
  }
}
