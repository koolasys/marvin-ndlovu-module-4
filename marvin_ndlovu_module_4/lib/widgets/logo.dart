import 'package:flutter/material.dart';

class AppLogo extends StatelessWidget {
  //double? size;
  const AppLogo({Key? key}) : super(key: key);

  //double? get getSize => size;

  @override
  Widget build(BuildContext context) {
    return const Image(
      image: AssetImage("assets/logo/logo.png"),
      width: 200,
      height: 200,
    );
  }
}
