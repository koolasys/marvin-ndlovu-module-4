import 'package:flutter/material.dart';

class Statistics extends StatelessWidget {
  const Statistics({Key? key}) : super(key: key);

  /// Statistics widget
  Widget buildStat(BuildContext context, String val, String contex) {
    return MaterialButton(
      onPressed: () {},
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            val,
            style: Theme.of(context).textTheme.headline2,
          ),
          const SizedBox(height: 5),
          Text(
            contex,
            style: Theme.of(context).textTheme.headline5,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildStat(context, "6", "Likes"),
        buildStat(context, "1", "Dislikes"),
        buildStat(context, "4", "Reviews"),
      ],
    );
  }
}
