import 'package:flutter/material.dart';

class TheMarket extends StatefulWidget {
  const TheMarket({Key? key}) : super(key: key);

  @override
  _TheMarketState createState() => _TheMarketState();
}

class _TheMarketState extends State<TheMarket> {
  final List<Map<String, dynamic>> _sellers = List.generate(
      100,
      (index) =>
          {"id": index, "name": "user{$index}", "iets": "Something else"});
  //final _stalls = <String>["Hi", "Word"];
  final List<Map<String, dynamic>> faved = [];

  @override
  Widget build(BuildContext context) {
    final alreadyFaved = faved.contains(_sellers);

    return Container(
        //height: double.infinity,
        //width: double.infinity,
        //padding: EdgeInsets.all(20),
        child: ListView(
            children: ListTile.divideTiles(
                color: Colors.red,
                context: context,
                tiles: _sellers.map((item) => ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.amber,
                        child: Text(item['id'].toString()),
                      ),
                      title: Text(item['name']),
                      subtitle: Text(item['iets']),
                      trailing: IconButton(
                        icon: Icon(
                          alreadyFaved ? Icons.favorite : Icons.favorite,
                          color: alreadyFaved ? Colors.red : null,
                        ),
                        onPressed: () => {
                          setState(() {
                            if (alreadyFaved) {
                              faved.remove(_sellers);
                            } else {
                              //faved.add(_sellers[item]);
                            }
                          })
                        },
                      ),
                    ))).toList()));

    /*  //Add a divider widget of 1px before each row in the ListView
        if (i.isOdd) {
          return Divider();
        }
        final int index = i ~/ 2;
        //To keep the list content generating on scroll down
        if (index >=
            _stalls.length) //_stalls is a list of sellers on the market
        {
          _stalls.addAll();
        }

        return _buildSeller(_stalls[index]);*/
  }

  //TBC
  /*Widget _buildStall() {
    return ListView.builder(
        padding: const EdgeInsets.all(16),
        itemBuilder: (BuildContext _context, int i) {
          //Add a divider widget of 1px before each row in the ListView
          if (i.isOdd) {
            return Divider(
                //color: Colors.black,
                );
          }
          final int index = i ~/ 2;
          //To keep the list content generating on scroll down
          if (index >=_stalls.length) //_stalls is a list of sellers on the market
          {
            //_stalls.addAll();
          }

          return _buildSeller(_stalls[index]);
        });
  }*/
}

Widget _buildSeller(_stalls) {
  return ListTile(
    leading: Icon(Icons.access_alarm),
    title: Text(
      "Alarm Icon",
      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
    ),
    subtitle: Text(
      "Just this shitty",
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
    ),
    isThreeLine: true,
    dense: false,
    onTap: () => print("Alarm pressed!!!!"),
    trailing: Icon(Icons.favorite_border),
  );
}

class HomeViewContent extends StatefulWidget {
  const HomeViewContent({Key? key}) : super(key: key);

  @override
  _HomeViewContentState createState() => _HomeViewContentState();
}

class _HomeViewContentState extends State<HomeViewContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.access_alarm),
              title: Text(
                "Alarm Icon",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                "Just this shit",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              isThreeLine: true,
              dense: false,
              onTap: () => print("Alarm pressed!!!!"),
              trailing: Icon(Icons.favorite_border),
            ),
            SizedBox(height: 20),
            Container(
              height: 100,
              color: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                child: ListTile(
                  leading: IconedPicture(), //Icon(Icons.access_alarm),
                  title: Text(
                    "Alarm Icon",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    "Just this shit",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  isThreeLine: true,
                  dense: false,
                  onTap: () => print("Alarm pressed!!!!"),
                  trailing: Icon(Icons.favorite_border),
                ),
              ),
            ),
            SizedBox(height: 20),
          ],
        ));
  }
}

class IconedPicture extends StatefulWidget {
  const IconedPicture({Key? key}) : super(key: key);

  @override
  _IconedPictureState createState() => _IconedPictureState();
}

class _IconedPictureState extends State<IconedPicture> {
  @override
  Widget build(BuildContext context) {
    return new ImageIcon(
      new AssetImage(
        'assets/icons/student2.png',
      ),
      size: 80,
    );
  }
}
/*
//Stateful widget for the home items list
class ContentList extends StatefulWidget {
  const ContentList({Key? key}) : super(key: key);

  @override
  _ContentListState createState() => _ContentListState();
}

class _ContentListState extends State<ContentList> {
  final _sellers = <String>[]; //This is a list
  final _faved = <String>{}; //This a set
  final _biggerFont = const TextStyle(fontSize: 18); //This is a style

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Add from here...
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: [
          IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
        ],
      ),
      body: _buildSuggestions(),
      backgroundColor: Colors.blue,
    ); // ... to here.
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16),
        // The itemBuilder callback is called once per seller item, and places each seller into a
        // ListTile row. For even rows, the function adds a ListTile row for each item. For odd rows,
        // the function adds a Divider widget to visually separate the entries. Note that the divider
        // may be difficult to see on smaller devices.
        itemBuilder: (BuildContext _context, int i) {
          // Add a one-pixel-high divider widget before each row
          // in the ListView.
          if (i.isOdd) {
            return Divider();
          }

          // The syntax "i ~/ 2" divides i by 2 and returns an
          // integer result.
          // For example: 1, 2, 3, 4, 5 becomes 0, 1, 1, 2, 2.
          // This calculates the actual number of word pairings
          // in the ListView,minus the divider widgets.
          final int index = i ~/ 2;
          // If you've reached the end of the available word
          // pairings...
          if (index >= _sellers.length) {
            // ...then generate 10 more and add them to the
            // suggestions list.
            _sellers.addAll(() {}.take(10));
          }
          return _buildRow(_sellers[index]);
        });
  }

  Widget _buildRow(WordPair pair) {
    final alreadySaved =
        _saved.contains(pair); //To ensure if a word pair has already been faved

    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(pair);
          } else {
            _saved.add(pair);
          }
        });
      },
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final tiles = _saved.map(
            (WordPair pair) {
              return ListTile(
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                ),
              );
            },
          );
          final divided = tiles.isNotEmpty
              ? ListTile.divideTiles(context: context, tiles: tiles).toList()
              : <Widget>[];

          return Scaffold(
            appBar: AppBar(
              title: Text('Saved Suggestions'),
            ),
            body: ListView(children: divided),
          );
        },
      ),
    );
  }
}
*/