import 'package:flutter/material.dart';
import 'package:marvin_ndlovu_module_4/routes/routes.dart';
import 'package:marvin_ndlovu_module_4/widgets/logo.dart';
import 'welcome_page.dart';

class SplashSc extends StatefulWidget {
  const SplashSc({Key? key}) : super(key: key);

  @override
  _SplashScState createState() => _SplashScState();
}

class _SplashScState extends State<SplashSc> {
  @override
  void initState() {
    super.initState();
    _toapp();
  }

  _toapp() async {
    await Future.delayed(const Duration(seconds: 2), () {});
    Navigator.pushReplacement(context, toWelcome());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: const Center(
          child: Hero(
            child: AppLogo(),
            tag: "intro-logo",
          ),
        ));
  }
}
