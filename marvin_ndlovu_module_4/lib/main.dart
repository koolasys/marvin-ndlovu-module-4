import 'package:flutter/material.dart';
import './widgets/opening.dart';
import 'themes/theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Smarket',
      theme: defaultTheme,
      home: const SplashSc(),
      debugShowCheckedModeBanner: false,
    );
  }
}
