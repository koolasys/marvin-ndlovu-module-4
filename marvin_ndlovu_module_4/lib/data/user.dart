class User {
  final String imgPath;
  final String name;
  final String surname;
  final String email;
  final String about;

  const User(
      {required this.imgPath,
      required this.name,
      required this.surname,
      required this.email,
      required this.about});
}
